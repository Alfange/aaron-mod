print("This file will be run at load time!")

minetest.register_node("aaron:speedblock", {
    description = "Ceci est un bloc pour aller vite",
    tiles = {"top.png",
            "side.png",
            "side.png",
            "side.png",
            "side.png",
            "side.png"
            },

    -- Casser le bloc --

    groups = { oddly_breakable_by_hand= 1},   

    -- Définir la fonction du bloc --

    on_punch = function(pos, node, puncher, pointed_thing)
        local playerspeed = puncher:get_physics_override().speed

            if playerspeed > 1 then
                puncher:set_physics_override({
                speed = 1,
            })
            elseif playerspeed == 1 then
                puncher:set_physics_override({
                    speed = 10,
            })
            end

        end,
    })

